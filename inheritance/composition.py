#   HAS-A содержит (композиция)
class Engine:
    print('Двигатель')


class Wheel:
    print('Колесо')


class Car:
    def __init__(self):
        self.engine = Engine()
        self.wheel = [Wheel()] * 4

    def start(self):
        self.engine.start()


car = Car()
if __name__ == '__main__':
    car

