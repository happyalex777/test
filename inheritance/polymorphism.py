#   полиморфизм
class MyList(list):
    def __str__(self):
        return super(MyList, self).__str__().replace(',', '\n')


my_list = MyList([1, 2, 3, 4, 5])

print(my_list)
