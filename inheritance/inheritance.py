#   IS-A является (наследование)


class Employee:
    def __init__(self, name, salary, bonus):
        self.name = name
        self.salary = salary
        self.bonus = bonus

    def calculator_bonus(self):
        return self.salary // 100 + self.bonus

    def __str__(self):
        return f'{self.__class__.__name__} {self.name} = {self.calculator_bonus()}'


class Cleaner(Employee):
    def __init__(self, name):
        super().__init__(name, 50000, 10)


class Boss(Employee):
    def __init__(self, name):
        super().__init__(name, 100000, 50)


c = Cleaner('Машка')
print(c)

b = Boss('Козлина')
print(b)
