class Cat(object):
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def meow(self):
        print(f'{self.name} says: Meow!')

    def __add__(self, other): # +
        if isinstance(other, Cat):
            return Cat('Ginger', 0)


tom_cat = Cat('Tom', 4)
angela_cat = Cat('Angela', 3)

tom_cat.meow()
angela_cat.meow()

ginger_cat = tom_cat + angela_cat

ginger_cat.meow()
