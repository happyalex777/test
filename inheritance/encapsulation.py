class MyClass:
    def __init__(self):
        self._admin = 'login'
        self.__password = 'password'


class MySonClass(MyClass):
    def __init__(self):
        super().__init__()
        self.__password = '12345'


my_class = MyClass()

# print(my_class._MyClass__password)
# print(my_class.__dir__())

my_son_class = MySonClass()
print(my_son_class._MySonClass__password)
print(my_son_class._MyClass__password)
print(my_son_class.__dir__())
